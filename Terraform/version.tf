terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.46"
    }

    random = {
      source  = "hashicorp/random"
      version = ">=3.4"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.16"
    }
  }
  required_version = "~> 1.3"
}

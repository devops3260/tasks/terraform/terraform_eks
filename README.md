### EKS project
This is simple project where we create Kubernets cluster in AWS. Also we create container regestry for save our images.

### Task
1. Use terraform create EKS and ECR in AWS
2. Build our application and push it in ECR
3. Create Helm chart for deploy in EKS
4. CI for build and push App
5. CD for deploy in kubernetes cluster


### Result

We created EKS and ECR with the help terraform. Created helm chart and wrote simple ci/cd pipelins for build and deploy app.

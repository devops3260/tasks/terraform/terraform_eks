locals {
  name = "tetris"
}

module "ecr" {
  source          = "terraform-aws-modules/ecr/aws"
  version         = "1.6.0"
  repository_name = local.name
  repository_type = "public"


  repository_read_write_access_arns = [data.aws_caller_identity.current.arn]
  repository_lifecycle_policy = jsonencode({
    rules = [
      {
        rulePriority = 1,
        description  = "Keep last 30 images",
        selection = {
          tagStaus      = "tagged",
          tagPrefixList = ["v"],
          countType     = "imagCountMoreThan",
          countNumber   = 30
        },
        action = {
          type = "expire"
        }
      }
    ]
  })


  public_repository_catalog_data = {
    description       = "Docker container for some things"
    operating_systems = ["Linux"]
    architectures     = ["x86"]
  }

  tags = {
    name = "Public-ecr"
  }
}
